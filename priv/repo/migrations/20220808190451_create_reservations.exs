defmodule Vask.Repo.Migrations.CreateReservations do
  use Ecto.Migration

  def change do
    create table(:reservations) do
      add :date, :date 
      add :apartment_id, references(:apartments, on_delete: :delete_all )
      add :schedule_id, references(:schedules, on_delete: :delete_all )
      add :deleted, :boolean

      timestamps()
    end

    create index(:reservations, [:date])
    create index(:reservations, [:schedule_id])
  end
end
