defmodule Vask.Repo.Migrations.CreateAdminsAuthTables do
  use Ecto.Migration

  def change do
    create table(:admins_tokens) do
      add :token, :binary, null: false, size: 32
      add :context, :string, null: false
      add :sent_to, :string
      timestamps(updated_at: false)
    end

    create unique_index(:admins_tokens, [:context, :token])
  end
end
