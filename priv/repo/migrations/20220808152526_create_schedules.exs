defmodule Vask.Repo.Migrations.CreateSchedules do
  use Ecto.Migration

  def change do
    create table(:schedules) do
      add :from, :time
      add :to, :time
      add :deleted, :boolean

      timestamps()
    end
  end
end
