defmodule Vask.Repo.Migrations.CreateHistory do
  use Ecto.Migration

  def change do
    create table(:reservation_history) do
      add :action, :string, null: false 
      add :reservation_id, references(:reservations, on_delete: :delete_all )
      timestamps()
    end
  end
end
