defmodule VaskWeb.PageController do
  use VaskWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
