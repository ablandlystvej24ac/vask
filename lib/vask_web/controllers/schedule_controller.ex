defmodule VaskWeb.ScheduleController do
  use VaskWeb, :controller

  alias Vask.Schedules
  alias Vask.Schedules.Schedule
  alias Ecto.Changeset


  def index(conn, _params) do
    schedules = Schedules.list_schedules()
    changeset = Schedules.change_schedule(%Schedule{})
    render(conn, "index.html", schedules: schedules, changeset: changeset)
  end

  def create(conn, %{"schedule" => schedule_params}) do
    case Schedules.create_schedule(schedule_params) do
      {:ok, _schedule} ->
        conn
        |> put_flash(:info, "Schedule created successfully.")
        |> redirect(to: Routes.schedule_path(conn, :index))

      {:error, :changeset, %Ecto.Changeset{} = changeset, s } ->
        schedules = Schedules.list_schedules()
        render(conn, "index.html", schedules: schedules, changeset: changeset)

      {:error, :overlap, overlapping_schedules, _} ->
        os = Enum.map(overlapping_schedules, fn s -> Time.to_string(s.from) <> " - " <> Time.to_string(s.to) end)
          |> Enum.intersperse(", \n")
        schedules = Schedules.list_schedules()
        changeset = Schedule.changeset(%Schedule{}, schedule_params)
          |> Changeset.add_error(:from, "Schedule overlaps the following schedules: #{os}")
        render(conn, "index.html", schedules: schedules, changeset: %{changeset | action: :insert} )

    end
  end

  def delete(conn, %{"id" => id}) do
    schedule = Schedules.get_schedule!(id)
    {:ok, _schedule} = Schedules.delete_schedule(schedule)

    conn
    |> put_flash(:info, "Schedule deleted successfully.")
    |> redirect(to: Routes.schedule_path(conn, :index))
  end
end
