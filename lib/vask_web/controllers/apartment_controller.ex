defmodule VaskWeb.ApartmentController do
  use VaskWeb, :controller

  alias Vask.Apartment
  alias Vask.Apartments

  def index(conn, _params) do
    apartments = Apartments.list()
    changeset = Apartments.change_apartment(%Apartment{})
    render(conn, "index.html", apartments: apartments, changeset: changeset)
  end

  def new(conn, _params) do
    changeset = Apartments.change_apartment(%Apartment{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"apartment" => apartment_params}) do
    case Apartments.create_apartment(apartment_params) do
      {:ok, _apartment} ->
        conn
        |> put_flash(:info, "created successfully.")
        |> redirect(to: Routes.apartment_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    apartment = Apartments.get_apartment(id)
    {:ok, _apartment} = Apartments.delete_apartment(apartment)

    conn
    |> put_flash(:info, "Apartment deleted successfully.")
    |> redirect(to: Routes.apartment_path(conn, :index))
  end
end
