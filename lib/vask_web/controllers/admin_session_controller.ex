defmodule VaskWeb.AdminSessionController do
  use VaskWeb, :controller

  alias VaskWeb.AdminAuth

  def new(conn, _params) do
    render(conn, "new.html", error_message: nil)
  end

  def create(conn, %{"admin" => admin_params}) do
    %{"password" => password} = admin_params

    hashed_password = Application.get_env(:vask, :admin_secret) 

    if is_binary(hashed_password) and byte_size(password) > 0 and Bcrypt.verify_pass(password, hashed_password) do
      AdminAuth.log_in_admin(conn, admin_params)
    else
      # In order to prevent user enumeration attacks, don't disclose whether the email is registered.
      render(conn, "new.html", error_message: "Invalid password")
    end
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> AdminAuth.log_out_admin()
  end
end
