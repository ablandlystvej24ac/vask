defmodule VaskWeb.ReservationController do
  use VaskWeb, :controller
  import Plug.Conn
  import Ecto.Query

  alias Vask.Reservations
  alias Vask.Reservation
  alias Vask.Repo
  alias Vask.Apartments
  alias Vask.Apartment
  alias Vask.Schedules.Schedule
  alias Vask.Schedules
  alias Vask.History
  alias Ecto.Multi

  action_fallback VaskWeb.FallbackController

  @max_age 1000 * 60 * 24 * 60
  @apartment_cookie "_vask_apartment"
  @apartment_options [sign: true, max_age: @max_age, same_site: "Lax"]

  defp now() do DateTime.now(Application.get_env(:elixir, :time_zone)) end

  def index(conn, _params)  do
    { my_apartment, conn } = ensure_apartment(conn)
    with {:ok, now} <- now() do
      my_apartment = if my_apartment do
        Apartments.get_apartment(my_apartment)
      end
      weekdate = DateTime.to_date(now)
      schedule = Reservations.week_schedule(now, weekdate)
      apartments = Apartments.list()
      week = %{my_apartment: my_apartment, apartments: apartments, schedule: schedule, weekstart: Date.beginning_of_week(weekdate)}
      render(conn, "index.html", week: week)
    end
  end

  def index_json(conn, _params)  do
    with {:ok, now} <- now() do
      weekdate = DateTime.to_date(now)
      schedule = Reservations.week_schedule(now, weekdate)
      apartments = Apartments.list()
      week = %{apartments: apartments, schedule: schedule, weekstart: Date.beginning_of_week(weekdate)}
      render(conn |> put_view(VaskWeb.ScheduleView), "index.json", week: week)
    end
  end

  def week_schedule(conn, %{"week" => week} ) do 
    with { :ok, date } <- Date.from_iso8601(week), 
         { :ok, now } <- now() do
      week = Reservations.week_schedule(now, date)
      render(conn, "week_schedule.json", reservation: week)
    end
  end


  def book(conn, params) do
    with {:ok, date } <- Date.from_iso8601(params["date"]),
         {:ok, now} <- now(),
         {:ok, res } <-
          Multi.new()
            |> Multi.one(:schedule, (from p in Schedule, where: p.id == (^params["schedule_id"])))
            |> Multi.one(:apartment, (from a in Apartment, where: a.id == (^params["apartment_id"])))
            |> Multi.run(:reservation, (fn repo, %{apartment: a, schedule: s} ->
                query = from r in Reservation, where: r.schedule_id == ^s.id and r.date == ^date and not r.deleted
                # Check that there there is no current reservation at the time. 
                # todo also check that there is no overlapping old schedules in case schedules were changed but bookings had been made.
                case repo.all(query) do
                  [] -> {:ok, "hell yeah" }
                  list -> {:error, "Dette tidspunkt er allerede booket."}
                end
              end)
              )
            |> Multi.run(:check_reservation_is_future, (fn _repo, %{ schedule: s } ->
              with {:ok, reservation_end} <- DateTime.new(date, s.to, Application.get_env(:elixir, :time_zone)) do
                  case DateTime.compare(reservation_end, now) do
                    :gt -> {:ok, ""}
                    _ -> {:error, "Du kan kun reservere tidspunkter i fremtiden."}
                  end
                end
              end)
              )
            |> Multi.run(:check_reservation_not_too_far_in_the_future, (fn _, _ ->
                now_date = DateTime.to_date(now) 
                limit = Date.add(now_date, Date.days_in_month(now)) 
                case Date.compare(limit, date) do
                  :gt -> { :ok, "" }
                  _ -> { :error, "Du kan ikke lave reservationer mere end en måned frem i tid." }
                end
              end)
              )
            |> Multi.run(:check_number_of_reservations_in_the_future, (fn repo, %{apartment: a} ->
                query = from r in Reservation,
                  join: s in assoc(r, :schedule), 
                  where: r.apartment_id == ^a.id 
                    and (r.date > ^(DateTime.to_date(now)) or ((r.date == ^(DateTime.to_date(now))) and (s.to > ^(DateTime.to_time(now))))) 
                    and (not r.deleted), preload: :schedule

                case Enum.count_until(repo.all(query), 3) do
                  3 -> {:error, "Du kan ikke lave mere end 3 rerservationer i fremtiden."}
                  _ -> {:ok, ""}
                end

              end)
              )
            |> Multi.insert(:insert, %Reservation{date: date, apartment_id: params["apartment_id"], schedule_id: params["schedule_id"]})
            |> Multi.insert(:history, (fn %{insert: reservation} -> 
              %History{action: "book", reservation_id: reservation.id } end )
            )
            |> Repo.transaction()
    do
      save_apartment(conn, res.apartment)
        |> render("book.json", result: res)
    else 
      { :error, :reservation, msg, _multi } -> 
        render(conn, "book_error.json", errormsg: msg)
      { :error, :check_number_of_reservations_in_the_future, msg, _multi } -> 
        render(conn, "book_error.json", errormsg: msg)
      { :error, :check_reservation_is_future, msg, _multi } -> 
        render(conn, "book_error.json", errormsg: msg)
      { :error, :check_reservation_not_too_far_in_the_future, msg, _multi } ->  
        render(conn, "book_error.json", errormsg: msg)
      err -> err
    end
  end

  def unbook(conn, params) do
    with {:ok, date } <- Date.from_iso8601(params["date"]),
         {:ok, res } <-
          Multi.new()
            |> Multi.one(:reservation, (from r in Reservation, where: r.schedule_id == (^params["schedule_id"]) and r.date == (^date) and not r.deleted))
            |> Multi.run(:unbook, (fn repo, %{reservation: r} -> 
              Reservations.delete_reservation(repo, r)
            end))
            |> Multi.insert(:history, (fn %{reservation: reservation} -> 
              %History{action: "unbook", reservation_id: reservation.id } end )
            )
            |> Repo.transaction()
    do
      render(conn, "unbook.json", result: res, schedule_id: params["schedule_id"], date: date)
    end
  end

  defp save_apartment(conn, apartment) do 
    put_session(conn, :my_apartment_id, apartment.id)
      |> put_resp_cookie(@apartment_cookie, apartment.id, @apartment_options)
  end 

  defp ensure_apartment(conn) do 
    if apartment = get_session(conn, :my_apartment_id) do
      {apartment, conn}
    else
      conn = fetch_cookies(conn, signed: [@apartment_cookie])

      if apartment = conn.cookies[@apartment_cookie] do
        {apartment, put_session(conn, :my_apartment_id, apartment)}
      else
        {nil, conn}
      end
    end
  end

  def history(conn, params) do 
      IO.inspect(params)
      skip = params["skip"]
      page_size = params["page_size"]
      history = History.list_history(skip, page_size)
      render(conn, "history_list.json", %{history: history } )
  end
end
