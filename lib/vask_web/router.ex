defmodule VaskWeb.Router do
  use VaskWeb, :router

  import VaskWeb.AdminAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {VaskWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_admin
  end

  pipeline :index do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {VaskWeb.IndexView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  scope "/", VaskWeb do
    pipe_through :index

    get "/", ReservationController, :index
  end

  scope "/admin", VaskWeb do
    pipe_through [:browser, :require_authenticated_admin]

    resources "/schedules", ScheduleController, except: [:show, :edit ]
    resources "/apartments", ApartmentController, only: [:index, :new, :create, :delete]
  end

  
  scope "/json", VaskWeb do
    pipe_through :api

    get "/", ReservationController, :index_json
    get "/weekschedule/:week", ReservationController, :week_schedule
    post "/book", ReservationController, :book
    post "/unbook", ReservationController, :unbook
    post "/history", ReservationController, :history
  end

  # Other scopes may use custom stacks.
  # scope "/api", VaskWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: VaskWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", VaskWeb do
    pipe_through [:browser, :redirect_if_admin_is_authenticated]

    get "/admin/log_in", AdminSessionController, :new
    post "/admin/log_in", AdminSessionController, :create
  end

  scope "/", VaskWeb do
    pipe_through [:browser]

    delete "/admin/log_out", AdminSessionController, :delete
  end
end
