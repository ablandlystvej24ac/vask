defmodule VaskWeb.ScheduleView do
  use VaskWeb, :view

  alias VaskWeb.ApartmentView
  alias VaskWeb.ReservationView

  def render("index.json", %{week: week}) do
    %{apartments: render_many(week.apartments, ApartmentView, "apartment.json"), 
      schedule: render_one(week.schedule, ReservationView, "week_schedule.json"),
      weekstart: week.weekstart,
      my_apartment: render_one(week.my_apartment, ApartmentView, "apartment.json")
    }
  end
end
