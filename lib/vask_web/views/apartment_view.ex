defmodule VaskWeb.ApartmentView do
  use VaskWeb, :view
  alias VaskWeb.ApartmentView


  def render("index.json", %{apartments: reservations}) do
    %{data: render_many(reservations, ApartmentView, "apartment.json")}
  end

  def render("show.json", %{apartment: reservation}) do
    %{data: render_one(reservation, ApartmentView, "apartment.json")}
  end

  def render("apartment.json", %{apartment: apartment}) do
    %{
      id: apartment.id,
      name: apartment.name
    }
  end

end
