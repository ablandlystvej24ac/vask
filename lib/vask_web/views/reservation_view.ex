defmodule VaskWeb.ReservationView do
  use VaskWeb, :view
  alias VaskWeb.ReservationView
  alias VaskWeb.ApartmentView


  def render("book.json", %{result: res}) do
    %{ apartment: render_one(res.apartment, ApartmentView, "apartment.json"),
       schedule_id: res.schedule.id,
       date: res.insert.date
    }

  end

  def render("book_error.json", %{errormsg: err}) do
    %{ error: err }
  end


  def render("reservation.json", %{reservation: reservation}) do
    %{ from: reservation.from, 
       to: reservation.to,
       booking: render_one(reservation.booking, ReservationView, "booking.json"),
       schedule_id: reservation.schedule_id
    }
  end

  def render("booking.json", %{reservation: res} ) do
    case res.status do 
      :available -> %{status: :available}
      :booked -> %{status: :booked, 
          apartment: render_one(res.apartment, ApartmentView, "apartment.json")
      }
    end
  end

  def render("unbook.json", %{result: _res, date: date, schedule_id: schedule_id } ) do
    %{ schedule_id: schedule_id,
       date: date 
     }
  end

  def render("week_schedule.json", %{reservation: week_schedule}) do
    Enum.map(week_schedule, fn {k, bookings} -> 
      {k, render_many(bookings, ReservationView, "reservation.json")} end)
      |> Enum.into(%{})
  end

  def render("history_list.json", %{history: history}) do 
    render_many(history, ReservationView, "history.json")
  end

  def render("history.json", %{reservation: history}) do 
    %{apartment: render_one(history.reservation.apartment, ApartmentView, "apartment.json"),
       from: history.reservation.schedule.from,
       to: history.reservation.schedule.to,
       happened: history.inserted_at,
       date: history.reservation.date,
       action: history.action
    }
  end
end
