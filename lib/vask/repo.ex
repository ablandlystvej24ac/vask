defmodule Vask.Repo do
  use Ecto.Repo,
    otp_app: :vask,
    adapter: Ecto.Adapters.SQLite3
end
