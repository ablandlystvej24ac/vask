defmodule Vask.History do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, warn: false 

  alias Vask.Repo

  schema "reservation_history" do
    field :action, :string
    belongs_to :reservation, Vask.Reservation
    timestamps()
  end

  @doc false
  def changeset(schedule, attrs) do
    schedule
    |> cast(attrs, [:action, :reservation_id])
    |> validate_required([:action, :reservation_id])
  end


  def list_history(skip, page_size) do 
    query =
      from s in Vask.History, order_by: [ desc: s.inserted_at ], limit: ^page_size, offset: ^skip, preload: [reservation: [:apartment, :schedule]]

    Repo.all(query)
  end

end
