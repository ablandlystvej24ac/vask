defmodule Vask.Schedules.Schedule do
  use Ecto.Schema
  import Ecto.Changeset

  schema "schedules" do
    field :from, :time
    field :to, :time
    field :deleted, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(schedule, attrs) do
    schedule
    |> cast(attrs, [:from, :to, :deleted])
    |> validate_required([:from, :to])
    |> validate_times
  end

  defp validate_times(changeset) do
    cond do
      get_field(changeset, :to) > get_field(changeset, :from) -> changeset
      true -> add_error(changeset, :to, "must be after from")
    end
  end



end
