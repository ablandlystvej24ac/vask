defmodule Vask.Admins do
  @moduledoc """
  The Admins context.
  """

  import Ecto.Query, warn: false
  alias Vask.Repo

  alias Vask.Admins.{AdminToken}


  ## Session

  @doc """
  Generates a session token.
  """
  def generate_admin_session_token() do
    {token, admin_token} = AdminToken.build_session_token()
    Repo.insert!(admin_token)
    token
  end

  @doc """
  Gets the admin with the given signed token.
  """
  def get_admin_by_session_token(token) do
    {:ok, query} = AdminToken.verify_session_token_query(token)
    Repo.one(query)
  end

  @doc """
  Deletes the signed token with the given context.
  """
  def delete_session_token(token) do
    Repo.delete_all(AdminToken.token_and_context_query(token, "admin_session"))
    :ok
  end
end
