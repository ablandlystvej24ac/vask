defmodule Vask.Reservations do
  @moduledoc """
  The Reservations context.
  """

  import Ecto.Query, warn: false
  alias Vask.Repo

  alias Vask.Reservation
  alias Vask.Apartments
  alias Vask.Apartment
  alias Vask.Schedules

  @doc """
  Returns the list of reservations.

  ## Examples

      iex> list_reservations()
      [%Reservation{}, ...]

  """
  def list_reservations do
    query = 
      from r in Reservation, where: not r.deleted
    Repo.all(query)
  end

  def reservations_between(before, later) do
    query =
      from s in Reservation, where: s.date >= ^before and s.date <= ^later and not s.deleted, preload: :apartment
 

    Repo.all(query)
  end


  def week_schedule(now, weekdate) do
    now_date = DateTime.to_date(now);
    now_time = DateTime.to_time(now);

    beginning_of_week = Date.beginning_of_week(weekdate)
    end_of_week = Date.end_of_week(weekdate)

    schedule = Schedules.list_schedules()
    {_, week} = 0..Date.diff(end_of_week, beginning_of_week)
           |> Enum.map(fn day -> Date.add(beginning_of_week, day) end)
           |> Enum.map(fn day -> 
                case Date.compare(day, now_date) do
                  :lt -> { day, [] }
                  :gt -> { day, schedule }
                  :eq -> { day, schedule }
                end
              end )
           |> Map.new()
           |> Map.get_and_update(now_date, fn day ->
               case day do 
                 nil -> :pop
                 _ -> { day, Enum.drop_while(schedule, fn s -> s.to < now_time end)} 
               end
             end )

    reservations = reservations_between(beginning_of_week, end_of_week)

    step2 = 
      week
      |> Map.new(fn { d, schedules} -> 
        { d, 
          Enum.map(schedules, fn s -> 
            %{ booking: %{status: :available }, schedule_id: s.id, from: s.from, to: s.to } end 
          )
        } 
        end)

    all_schedules = Schedules.all_schedules()
    
    Enum.reduce(reservations, step2, (fn r, acc -> 
      Map.update!(acc, r.date, fn item -> 
        if Enum.any?(item, fn s -> 
          s.schedule_id == r.schedule_id end)
          do 
            Enum.reduce(item, [], (fn s, acc -> 
              if s.schedule_id == r.schedule_id do
                [ %{ s | booking: %{status: :booked, apartment: r.apartment} } | acc ] 

              else 
                [ s | acc ] 

              end
            end))
          else
            s = Enum.find(all_schedules, fn s1 -> s1.id == r.schedule_id end )
            [ %{ booking: %{status: :booked, apartment: r.apartment}, schedule_id: r.schedule_id, from: s.from, to: s.to}  | item ]
          end 
        end)
      end)
    )
  end


  @doc """
  Gets a single reservation.

  Raises if the Reservation does not exist.

  ## Examples

      iex> get_reservation!(123)
      %Reservation{}

  """
  def get_reservation!(id, repo), do: repo.get!(Reservation, id)


  @doc """
  Deletes a Reservation.

  ## Examples

      iex> delete_reservation(reservation)
      {:ok, %Reservation{}}

      iex> delete_reservation(reservation)
      {:error, ...}

  """
  def delete_reservation(repo, %Reservation{} = reservation) do
    reservation
    |> Reservation.changeset(%{:deleted=> true})
    |> repo.update()
  end
end
