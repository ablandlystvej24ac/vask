defmodule Vask.Schedules do
  @moduledoc """
  The Schedules context.
  """

  import Ecto.Query, warn: false 
  alias Ecto.Multi
  alias Ecto.Changeset

  alias Vask.Repo

  alias Vask.Schedules.Schedule

  @doc """
  Returns the list of schedules.

  ## Examples

      iex> list_schedules()
      [%Schedule{}, ...]

  """
  def list_schedules do
    query = 
      from s in Schedule, where: not s.deleted, order_by: s.from

    Repo.all(query)
  end

  def all_schedules do
    query = 
      from s in Schedule, order_by: s.from

    Repo.all(query)
  end



  def overlaps_with(schedule) do 
    from s in Schedule, 
      where: ^schedule.id != s.id and (not s.deleted) and (not ((^schedule.from < s.from and ^schedule.to <= s.from) or (^schedule.from >= s.to and ^schedule.to > s.to)))
  end

  @doc """
  Gets a single schedule.

  Raises `Ecto.NoResultsError` if the Schedule does not exist.

  ## Examples

      iex> get_schedule!(123)
      %Schedule{}

      iex> get_schedule!(456)
      ** (Ecto.NoResultsError)

  """
  def get_schedule!(id), do: Repo.get!(Schedule, id)

  @doc """
  Creates a schedule.

  ## Examples

      iex> create_schedule(%{field: value})
      {:ok, %Schedule{}}

      iex> create_schedule(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_schedule(attrs \\ %{}) do
    changeset = Schedule.changeset(%Schedule{}, attrs)
    Multi.new()
      |> Multi.insert(:changeset, changeset)
      |> Multi.run(:overlap, fn repo, changes ->
        case repo.all(overlaps_with(changes.changeset)) do
          [] -> {:ok, changes.changeset }
          list -> {:error, list }
        end
      end)
      |> Repo.transaction()
  end

  @doc """
  Updates a schedule.

  ## Examples

      iex> update_schedule(schedule, %{field: new_value})
      {:ok, %Schedule{}}

      iex> update_schedule(schedule, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_schedule(%Schedule{} = schedule, attrs) do
    schedule
    |> Schedule.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a schedule.

  ## Examples

      iex> delete_schedule(schedule)
      {:ok, %Schedule{}}

      iex> delete_schedule(schedule)
      {:error, %Ecto.Changeset{}}

  """
  def delete_schedule(%Schedule{} = schedule) do
    schedule
    |> Schedule.changeset(%{:deleted=> true})
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking schedule changes.

  ## Examples

      iex> change_schedule(schedule)
      %Ecto.Changeset{data: %Schedule{}}

  """
  def change_schedule(%Schedule{} = schedule, attrs \\ %{}) do
    Schedule.changeset(schedule, attrs)
  end
end
