defmodule Vask.Apartments do
  @moduledoc """
  The Schedules context.
  """

  import Ecto.Query, warn: false
  alias Vask.Repo

  alias Vask.Apartment

  @doc """
  Returns the list of apartments.

  ## Examples

      iex> list()
      [%Apartment{}, ...]

  """
  def list do
    Repo.all(Apartment)
  end

  def change_apartment(%Apartment{} = apartment, attrs \\ %{}) do
    Apartment.changeset(apartment, attrs)
  end

  @doc """
  Creates an apartment.
  """
  def create_apartment(attrs \\ %{}) do
    %Apartment{}
    |> Apartment.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Gets a single apartmetn.

  Raises `Ecto.NoResultsError` if the Aparmtent does not exist.

  """
  def get_apartment(id), do: Repo.get!(Apartment, id)


  @doc """
  Deletes an apartment.

  """
  def delete_apartment(%Apartment{} = apartment) do
    Repo.delete(apartment)
  end
end
