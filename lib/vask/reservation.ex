defmodule Vask.Reservation do
  use Ecto.Schema
  import Ecto.Changeset

  schema "reservations" do
    field :date, :date
    belongs_to :apartment, Vask.Apartment
    belongs_to :schedule, Vask.Schedules.Schedule
    field :deleted, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(reservation, attrs) do
    reservation
    |> cast(attrs, [:date, :deleted, :apartment_id, :schedule_id ])
    |> validate_required([:date, :apartment_id, :schedule_id])
  end
end
