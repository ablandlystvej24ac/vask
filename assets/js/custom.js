customElements.define('elm-carousel',
    class extends HTMLElement {
        constructor() { 
            super(); 
            this._alive = false;
            this._requestFrame = null;
        }
        connectedCallback() { 
            this._alive = true;

            const scrolled = (index) => {
                console.log("scroll: " + index);
                if (this._requestFrame != null) {
                    if (index == this._requestFrame) {
                        this._requestFrame = null
                    }
                } else if (index != this._index) {
                    this._index = index;
                    console.log( "what:" + index);
                    this.dispatchEvent( new CustomEvent("index-changed")); 
                }
            };

            this.addEventListener('scroll', (event) => { 
                const scrollLeftMax = event.target.scrollWidth - event.target.clientWidth;
                const position = (this._pages / scrollLeftMax) * event.target.scrollLeft;
                const index = Math.round(position);
                console.log(index);
                clearTimeout(this._timer);
                this._timer = setTimeout( () => scrolled(index), 60);
            });
        }
        attributeChangedCallback(name, oldValue, newValue) { 
            const oldIndex = this._index
            switch (name)
            { case "index": this._index = newValue; break;
              case "pages": this._pages = newValue; break;
            }

            if (this._alive && name == "index") {
                if ( oldIndex != this._index ) {
                    const destination = ((this.scrollWidth - this.clientWidth) / this._pages) * this._index;
                    console.log(destination);
                    this._requestFrame = this._index;
                    this.scrollTo(
                        { top: 0, 
                          left: destination,
                          behavior: "smooth"
                        });
                }
            }
//            if (this._alive) {
//                if (name == "index") {
//                    this.scrollTo(
//                        { top: 0, 
//                          left: (this.scrollWidth - this.clientWidth / this._pages) * this._index, 
//                          behavior: "smooth"
//                        }
//                    );
//                    if (oldValue != newValue && oldIndex != newValue ) {
//                        this._requestFrame = this._index;
//                    }
//                }
//            }
        }
        static get observedAttributes() { return ['index', 'pages']; }
    }
);

