module Main exposing (Msg(..), init, main, subscriptions, update, view)

import Apartment
import Browser
import Carousel
import Clock
import Color
import Date
import Dict exposing (Dict)
import History exposing (History)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as D
import Json.Decode.Pipeline as D
import Json.Encode as E
import List.Extra as List
import Material.Icons
import Material.Icons.Outlined
import Material.Icons.Types exposing (..)
import Schedule
import Tachyons exposing (TachyonsMedia)
import Time



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type alias APIResult a =
    Result APIError a


type APIError
    = HttpError Http.Error
    | APIError String


type alias RataDie =
    Int


type alias ScheduleId =
    Int


type alias Week =
    Dict RataDie (Dict ScheduleId Schedule.Schedule)


type alias Model =
    { apartments : List Apartment.Apartment
    , myApartment : Maybe Apartment.Apartment
    , apartmentSelection : Maybe (Apartment.Apartment -> Msg)
    , currentWeek : Date.Date
    , carouselIdx : Int
    , weeks : Dict Int Week
    , history : Dict Int (List History)
    , viewHistory : Maybe Int
    , host : String
    , tachyonsMedia : TachyonsMedia
    , flashMsg : Maybe String
    }


weekDecoder =
    D.dict (D.list Schedule.decoder)
        |> D.map
            (\dict ->
                Dict.toList dict
                    |> List.filterMap
                        (\( key, val ) ->
                            case Date.fromIsoString key of
                                Ok date ->
                                    Just
                                        ( Date.toRataDie date
                                        , List.map (\s -> ( s.scheduleId, s )) val
                                            |> Dict.fromList
                                        )

                                Err _ ->
                                    Nothing
                        )
                    |> Dict.fromList
            )


errorDecoder =
    D.field "error" D.string


bookingDecoder : D.Decoder (Result String Booking)
bookingDecoder =
    D.oneOf
        [ D.map Ok <|
            D.map3 Booking
                (D.field "apartment" Apartment.decoder)
                (D.field "schedule_id" D.int)
                (D.field "date" Schedule.dateDecoder)
        , D.map Err errorDecoder
        ]


flagsDecoder =
    D.succeed Model
        |> D.required "apartments"
            (D.list Apartment.decoder)
        |> D.optional "my_apartment" (D.maybe Apartment.decoder) Nothing
        |> D.hardcoded Nothing
        |> D.required "weekstart" Schedule.dateDecoder
        |> D.hardcoded 0
        |> D.required "schedule"
            (D.map
                (\week -> Dict.singleton 0 week)
                weekDecoder
            )
        |> D.hardcoded Dict.empty
        |> D.hardcoded Nothing
        |> D.required "host" D.string
        |> D.required "tachyonsMedia" Tachyons.decoder
        |> D.hardcoded Nothing


init : E.Value -> ( Model, Cmd Msg )
init value =
    let
        week =
            D.decodeValue flagsDecoder value
    in
    case week of
        Ok res ->
            ( res
            , Cmd.none
            )

        Err e ->
            let
                _ =
                    Debug.log "e" e
            in
            ( { apartments = []
              , myApartment = Nothing
              , apartmentSelection = Nothing
              , weeks = Dict.empty
              , carouselIdx = 0
              , currentWeek = Date.fromRataDie 0
              , history = Dict.empty
              , viewHistory = Nothing
              , host = "unknown"
              , tachyonsMedia =
                    { ns = False
                    , m = False
                    , l = False
                    }
              , flashMsg = Nothing
              }
            , Cmd.none
            )



-- UPDATE


type Msg
    = Book Apartment.Apartment Int Date.Date
    | GotBooking Int Date.Date ScheduleId (APIResult Booking)
    | UnBook ScheduleId Date.Date
    | GotUnBooking Int Date.Date ScheduleId (Result Http.Error Int)
    | GotWeek Int (Result Http.Error Week)
    | Navigate Navigate
    | ScrollTo Int
    | NoOp
    | ChooseApartmentFor (Maybe (Choose Apartment.Apartment)) (Apartment.Apartment -> Msg)
    | SetMyApartment Apartment.Apartment
    | History (Maybe Int)
    | GotHistory Int (Result Http.Error (List History))
    | GotTachyonsMedia TachyonsMedia
    | SetFlash (Maybe String)


type alias Booking =
    { apartment : Apartment.Apartment
    , schedule_id : ScheduleId
    , date : Date.Date
    }


type Choose a
    = Cancel
    | Choose a


type Navigate
    = Forward
    | Back


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        updateSchedule map cid date schedule_id =
            Dict.update cid
                (Maybe.map
                    (Dict.update
                        (Date.toRataDie date)
                        (Maybe.map
                            (Dict.update schedule_id
                                (Maybe.map map)
                            )
                        )
                    )
                )
    in
    case msg of
        Book apartment schedule_id date ->
            let
                mapAPIError : (APIResult a -> msg) -> Result Http.Error (Result String a) -> msg
                mapAPIError m r =
                    Result.mapError HttpError r
                        |> Result.andThen
                            (\api ->
                                Result.mapError APIError api
                            )
                        |> m
            in
            ( { model
                | weeks =
                    model.weeks
                        |> updateSchedule (\s -> { s | status = Schedule.Loading })
                            model.carouselIdx
                            date
                            schedule_id
              }
            , Http.post
                { url = model.host ++ "/json/book"
                , body =
                    Http.jsonBody
                        (E.object
                            [ ( "schedule_id", E.int schedule_id )
                            , ( "date", E.string (Date.toIsoString date) )
                            , ( "apartment_id", E.int apartment.id )
                            ]
                        )
                , expect =
                    Http.expectJson (mapAPIError (GotBooking model.carouselIdx date schedule_id))
                        bookingDecoder
                }
            )

        GotBooking cid date schedule_id (Ok book) ->
            ( { model
                | weeks =
                    model.weeks
                        |> updateSchedule (\s -> { s | status = Schedule.Booked book.apartment })
                            cid
                            date
                            schedule_id
                , myApartment = Just book.apartment
              }
            , Cmd.none
            )

        GotBooking cid date schedule_id (Err e) ->
            ( { model
                | weeks =
                    model.weeks
                        |> updateSchedule (\s -> { s | status = Schedule.Error })
                            cid
                            date
                            schedule_id
                , flashMsg =
                    case e of
                        APIError errormsg ->
                            Just errormsg

                        _ ->
                            Nothing
              }
            , Cmd.none
            )

        UnBook schedule_id date ->
            ( { model
                | weeks =
                    model.weeks
                        |> updateSchedule (\s -> { s | status = Schedule.Loading })
                            model.carouselIdx
                            date
                            schedule_id
              }
            , Http.post
                { url = model.host ++ "/json/unbook"
                , body =
                    Http.jsonBody
                        (E.object
                            [ ( "schedule_id", E.int schedule_id )
                            , ( "date", E.string (Date.toIsoString date) )
                            ]
                        )
                , expect =
                    Http.expectJson (GotUnBooking model.carouselIdx date schedule_id)
                        (D.succeed 1)
                }
            )

        GotWeek idx (Ok week) ->
            ( { model | weeks = Dict.insert idx week model.weeks }
            , Cmd.none
            )

        GotUnBooking cid date schedule_id (Ok _) ->
            ( { model
                | weeks =
                    model.weeks
                        |> updateSchedule (\s -> { s | status = Schedule.Available })
                            cid
                            date
                            schedule_id
              }
            , Cmd.none
            )

        GotUnBooking cid date schedule_id (Err e) ->
            ( { model
                | weeks =
                    model.weeks
                        |> updateSchedule (\s -> { s | status = Schedule.Error })
                            cid
                            date
                            schedule_id
              }
            , Cmd.none
            )

        GotWeek _ (Err _) ->
            Debug.todo ""

        Navigate nav ->
            let
                indx =
                    case nav of
                        Forward ->
                            model.carouselIdx + 1

                        Back ->
                            Basics.max 0 (model.carouselIdx - 1)

                cmd =
                    case Dict.get indx model.weeks of
                        Nothing ->
                            let
                                date =
                                    Date.add Date.Weeks indx model.currentWeek
                            in
                            Http.get
                                { url = model.host ++ "/json/weekschedule/" ++ Date.toIsoString date
                                , expect =
                                    Http.expectJson (GotWeek indx) weekDecoder
                                }

                        Just _ ->
                            Cmd.none
            in
            ( { model | carouselIdx = indx }, cmd )

        NoOp ->
            ( model, Cmd.none )

        ScrollTo indx ->
            let
                cmd =
                    case Dict.get indx model.weeks of
                        Nothing ->
                            let
                                date =
                                    Date.add Date.Weeks indx model.currentWeek
                            in
                            Http.get
                                { url = model.host ++ "/json/weekschedule/" ++ Date.toIsoString date
                                , expect =
                                    Http.expectJson (GotWeek indx) weekDecoder
                                }

                        Just _ ->
                            Cmd.none
            in
            ( { model | carouselIdx = indx }, cmd )

        ChooseApartmentFor ma toMsg ->
            case ma of
                Just Cancel ->
                    ( { model | apartmentSelection = Nothing }, Cmd.none )

                Just (Choose a) ->
                    update (toMsg a) { model | apartmentSelection = Nothing }

                Nothing ->
                    ( { model | apartmentSelection = Just toMsg }
                    , Cmd.none
                    )

        SetMyApartment apartment ->
            ( { model | myApartment = Just apartment }
            , Cmd.none
            )

        History page ->
            let
                page_size =
                    10
            in
            case page of
                Just p ->
                    ( { model | viewHistory = page }
                    , Http.post
                        { url = model.host ++ "/json/history"
                        , body =
                            Http.jsonBody
                                (E.object
                                    [ ( "skip", E.int (p * page_size) )
                                    , ( "page_size", E.int page_size )
                                    ]
                                )
                        , expect =
                            Http.expectJson (GotHistory p)
                                (D.list History.decoder)
                        }
                    )

                Nothing ->
                    ( { model | viewHistory = page }
                    , Cmd.none
                    )

        GotHistory page (Ok list) ->
            ( { model | history = Dict.insert page list model.history }
            , Cmd.none
            )

        GotHistory page (Err e) ->
            Debug.todo "error"

        GotTachyonsMedia media ->
            ( { model | tachyonsMedia = media }
            , Cmd.none
            )

        SetFlash flash ->
            ( { model | flashMsg = flash }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Tachyons.getMedia GotTachyonsMedia



-- VIEW


clockToMinutes : Clock.Clock -> Float
clockToMinutes clock =
    Basics.toFloat (clock.hours * 60) + Basics.toFloat clock.minutes + (Basics.toFloat clock.seconds / 60)


weekday : Time.Weekday -> String
weekday week =
    case week of
        Time.Mon ->
            "Mandag"

        Time.Tue ->
            "Tirsdag"

        Time.Wed ->
            "Onsdag"

        Time.Thu ->
            "Torsdag"

        Time.Fri ->
            "Fredag"

        Time.Sat ->
            "Lørdag"

        Time.Sun ->
            "Søndag"


month : Time.Month -> String
month m =
    case m of
        Time.Jan ->
            "Januar"

        Time.Feb ->
            "Februar"

        Time.Mar ->
            "Marts"

        Time.Apr ->
            "April"

        Time.May ->
            "Maj"

        Time.Jun ->
            "Juni"

        Time.Jul ->
            "Juli"

        Time.Aug ->
            "August"

        Time.Sep ->
            "September"

        Time.Oct ->
            "Oktober"

        Time.Nov ->
            "November"

        Time.Dec ->
            "December"


viewBooking model s date =
    let bookEventListener =
                case model.myApartment of
                    Just a ->
                        onClick (Book a s.scheduleId date)

                    Nothing ->
                        onClick (ChooseApartmentFor Nothing (\a -> Book a s.scheduleId date))

    in
    case s.status of
        Schedule.Available ->
            div
                [ class "pointer bg-animate bg-green b--dark-green ba h-100 pa1  br1"
                , bookEventListener    
                ]
                [ div
                    [ class "relative h-100 w-100 light-gray" ]
                    [ if model.tachyonsMedia.l then
                        span [ class "absolute top-0 right-0 f5 fw4 mb2 self-end" ] [ text "Ledig" ]

                      else
                        text ""
                    , span
                        [ class "absolute h-100 w-100 z-1 f6 f5-ns f4-l fw9-ns link dim dib pointer flex items-center justify-center"
                        , style "flex" "1"
                        ]
                        [ text "Reserver" ]
                    , if not model.tachyonsMedia.ns then
                        div [ class "tr w-100" ]
                            [ Material.Icons.add_circle 24 Inherit ]

                      else
                        text ""
                    ]
                ]

        Schedule.Booked a ->
            let
                ( char, xs ) =
                    ( String.left 1 a.name, String.dropLeft 1 a.name )
            in
            div [ class "bg-animate gold bg-red b--dark-red ba h-100 pa1 overflow-hidden flex flex-column br1" ]
                [ div
                    [ class "relative h-100 w-100" ]
                    [ div
                        [ class "absolute top-0 right-0 flex items-center flex-wrap-reverse" ]
                        [ if model.tachyonsMedia.l then
                            span [ class "f5 fw4 mr2" ] [ text "Optaget" ]

                          else
                            text ""
                        , case model.myApartment of
                            Just ma ->
                                if ma.id == a.id then
                                    span [ onClick (UnBook s.scheduleId date), class "pointer" ] [ Material.Icons.close 24 Inherit ]

                                else
                                    text ""

                            Nothing ->
                                text ""
                        ]
                    , div [ class "absolute left-0 bottom-0 flex items-baseline" ]
                        [ span [ class "f4 f1-l fw7 mr1" ] [ text char ]
                        , span [ class "f5 f4-l" ] [ text xs ]
                        ]
                    ]
                ]

        Schedule.Loading ->
            div [ class "bg-animate moon-gray o-70 bg-gray h-100 pa1 overflow-hidden flex flex-column justify-center items-center" ]
                [ Material.Icons.hourglass_top 32 Inherit ]

        Schedule.Error ->
            div [ class "bg-animate pointer dark-red bg-yellow b--dark-red ba h-100 pa1 overflow-hidden flex flex-column justify-center items-center" 
                , bookEventListener    
                ]
                [ Material.Icons.warning 32 Inherit ]


viewWeek : Model -> String -> Week -> Html Msg
viewWeek model id week =
    let
        weekValues =
            Dict.values week

        maxMinutes =
            List.filterMap
                (Dict.values
                    >> List.map (.to >> clockToMinutes)
                    >> List.maximum
                )
                weekValues
                |> List.maximum
                |> Maybe.withDefault 1

        minMinutes =
            List.filterMap
                (Dict.values
                    >> List.map (.from >> clockToMinutes)
                    >> List.minimum
                )
                weekValues
                |> List.minimum
                |> Maybe.withDefault 0

        times =
            List.map
                (Dict.values
                    >> List.map (\t -> [ t.from, t.to ])
                    >> List.concat
                )
                weekValues
                |> List.concat
                |> List.map (\c -> ( Clock.toString c, c ))
                |> Dict.fromList
                |> Dict.toList
                |> List.map
                    (\( t, c ) ->
                        ( t, c, (clockToMinutes c - minMinutes) / (maxMinutes - minMinutes) * 100 )
                    )

        timesView =
            span
                [ class "f5 f4-m f3-l ma2"
                , style "visibility" "hidden"
                ]
                [ text "00:00" ]
                :: (times
                        |> List.map
                            (\( t, _, top ) ->
                                span
                                    [ class "absolute f5 f4-m f3-l"
                                    , style "top" (String.fromFloat top ++ "%")
                                    , style "left" "50%"
                                    , style "transform" "translate(-50%, -50%)"
                                    ]
                                    [ text t ]
                            )
                   )
                |> div [ class "h-100 relative", style "grid-column" "1", style "grid-row" "2" ]

        overlay =
            div
                [ class "relative"
                , style "grid-column" "2 / span 7"
                , style "grid-row" "2"
                ]
                (times
                    |> List.map
                        (\( _, _, top ) ->
                            div [ class "absolute w-100 bt b--black-40", style "top" (String.fromFloat top ++ "%") ]
                                []
                        )
                )
    in
    div
        [ class "h-100 pb3"
        , style "min-width" "max(100vw, 46em)"
        , style "min-height" "max(100%, 40em)"
        , style "width" "100vw"
        , style "max-width" "100vw"
        , style "display" "grid"
        , style "scroll-snap-align" "start"
        , style "scroll-snap-stop" "always"
        , style "grid-template-columns" "auto repeat(7, 1fr)"
        , style "grid-template-rows" "auto 1fr"
        ]
        (timesView
            :: overlay
            :: (List.concat <|
                    List.indexedMap
                        (\i ( rddate, col ) ->
                            let
                                date =
                                    Date.fromRataDie rddate
                            in
                            [ div
                                [ style "grid-column" (String.fromInt <| 2 + i)
                                , style "grid-row" "1"
                                , class "flex flex-column"
                                ]
                                [ span
                                    [ class "f4 f3-l fw9" ]
                                    [ Date.weekday
                                        date
                                        |> weekday
                                        |> text
                                    , text " "
                                    ]
                                , span [ class "f5 nowrap mb2" ]
                                    [ Date.day date
                                        |> (\d -> String.fromInt d ++ ".")
                                        |> text
                                    , text " "
                                    , Date.month
                                        date
                                        |> month
                                        |> text
                                    ]
                                ]
                            , div
                                [ class "h-100 relative br b--black-40"
                                , style "grid-column" (String.fromInt <| 2 + i)
                                , style "grid-row" "2"
                                ]
                                (List.map
                                    (\s ->
                                        let
                                            top =
                                                (clockToMinutes s.from - minMinutes) / (maxMinutes - minMinutes) * 100

                                            height =
                                                (clockToMinutes s.to - clockToMinutes s.from) / (maxMinutes - minMinutes) * 100
                                        in
                                        div
                                            [ class "absolute w-100 pa1"
                                            , style "height" (String.fromFloat height ++ "%")
                                            , style "top" (String.fromFloat top ++ "%")
                                            ]
                                            [ viewBooking model s date ]
                                    )
                                    (Dict.values col)
                                )
                            ]
                        )
                        (Dict.toList week |> List.sortBy Tuple.first)
               )
        )


view : Model -> Html Msg
view model =
    section
        [ style "height" "100%"
        , class "flex flex-column"
        ]
        [ let
            size =
                if model.tachyonsMedia.l then
                    40

                else
                    28
          in
          div
            [ class "flex justify-between" ]
            [ div
                [ class "link pa2 dim dib dark-gray pointer bw1 b--dark-gray flex items-center justify-center"
                , style "display" "block"
                , onClick <| Navigate Back
                ]
                [ if model.carouselIdx == 0 then
                    text ""

                  else
                    Material.Icons.arrow_back size Inherit
                ]
            , div
                [ class "link pa2 dim dib dark-gray pointer bw1 b--dark-gray flex items-center justify-center"
                , style "display" "block"
                , onClick <| ChooseApartmentFor Nothing SetMyApartment
                ]
                [ Material.Icons.Outlined.door_front size Inherit ]
            , div
                [ class "link pa2 dim dib dark-gray pointer bw1 b--dark-gray flex items-center justify-center"
                , style "display" "block"
                , onClick <| History (Just 0)
                ]
                [ Material.Icons.Outlined.manage_search size Inherit ]
            , div
                [ class "link pa2 dim dib dark-gray pointer bw1 b--dark-gray flex items-center justify-center"
                , style "display" "block"
                , onClick <| Navigate Forward
                ]
                [ Material.Icons.arrow_forward size Inherit ]
            ]
        , case model.flashMsg of
            Just msg ->
                div [ class "center pv3" ]
                    [ span [ class "relative pv3 ph4 f3 bg-light-red dark-red br2 flex items-center" ]
                        [ span [ class "mr2" ] [ Material.Icons.Outlined.info 28 Inherit ]
                        , text msg
                        , div
                            [ class "absolute db pointer"
                            , style "top" "6px"
                            , style "right" "10px"
                            , onClick (SetFlash Nothing)
                            ]
                            [ Material.Icons.close 20 Inherit ]
                        ]
                    ]

            Nothing ->
                text ""
        , case model.viewHistory of
            Just page ->
                div
                    [ style "position" "fixed"
                    , style "z-index" "2"
                    , class "h-100 w-100 bg-dark-blue moon-gray"
                    ]
                    [ div
                        [ class "w-100 h-100 mw8 center flex flex-column justify-center" ]
                        [ h2 [ class "f2 mt3 flex justify-between items-center" ] [ text "Historik", div [ class "pointer ma3", onClick (History Nothing) ] [ Material.Icons.close 32 Inherit ] ]
                        , div
                            [ class "overflow-auto" ]
                            [ table [ class "w-100 center" ]
                                [ thead []
                                    [ tr []
                                        [ th [ class "fw6 bb b--moon-gray tl pb3 pr3" ] [ text "Lejlighed" ]
                                        , th [ class "fw6 bb b--moon-gray tl pb3 pr3" ] [ text "Hændelse" ]
                                        , th [ class "fw6 bb b--moon-gray tl pb3 pr3" ] [ text "Booking" ]
                                        , th [ class "fw6 bb b--moon-gray tl pb3 pr3" ] [ text "Tidspunkt" ]
                                        ]
                                    ]
                                , tbody []
                                    (case Dict.get page model.history of
                                        Just hlist ->
                                            List.map
                                                (\h ->
                                                    tr []
                                                        [ td [ class "f4 pv3 pr3 bb b--moon-gray" ] [ text h.apartment.name ]
                                                        , td [ class "f4 pv3 pr3 bb b--moon-gray" ]
                                                            [ text <|
                                                                case h.action of
                                                                    History.Booked ->
                                                                        "Reserverede"

                                                                    History.UnBooked ->
                                                                        "Fjernet"
                                                            ]
                                                        , td [ class "f4 pv3 pr3 bb b--moon-gray nowrap" ]
                                                            [ Date.day h.date
                                                                |> (\d -> String.fromInt d ++ ".")
                                                                |> text
                                                            , text " "
                                                            , Date.month
                                                                h.date
                                                                |> month
                                                                |> text
                                                            , text " "
                                                            , text <| Clock.toString h.from
                                                            , text " - "
                                                            , text <| Clock.toString h.to
                                                            ]
                                                        , td [ class "f4 pv3 pr3 bb b--moon-gray" ] [ text h.happened ]
                                                        ]
                                                )
                                                hlist

                                        Nothing ->
                                            []
                                    )
                                ]
                            ]
                        , div [ class "flex justify-between" ]
                            [ div
                                [ class "link pa2 dim dib moon-gray pointer bw1 b--dark-gray flex items-center justify-center"
                                , style "display" "block"
                                , onClick <| History (Just (Basics.max 0 (page - 1)))
                                ]
                                (if page == 0 then
                                    []

                                 else
                                    [ Material.Icons.arrow_back 40 Inherit ]
                                )
                            , div
                                [ class "link pa2 dim dib moon-gray pointer bw1 b--dark-gray flex items-center justify-center"
                                , style "display" "block"
                                , onClick <| History (Just (page + 1))
                                ]
                                [ Material.Icons.arrow_forward 40 Inherit ]
                            ]
                        ]
                    ]

            Nothing ->
                text ""
        , case model.apartmentSelection of
            Just toMsg ->
                div
                    [ style "position" "fixed"
                    , style "z-index" "2"
                    , class "h-100 w-100 bg-dark-green moon-gray"
                    ]
                    [ div
                        [ class "w-100 h-100 mt3 mw6 center flex flex-column justify-center" ]
                        [ h2 [ class "f2 mt3 flex justify-between items-center" ] [ text "Vælg Lejlighed", div [ class "pointer ma3", onClick <| ChooseApartmentFor (Just Cancel) toMsg ] [ Material.Icons.close 32 Inherit ] ]
                        , ul [ class "w-100 ma0 list pl0 ml0 ba b--light-silver br2 overflow-auto" ]
                            (List.map
                                (\a ->
                                    li
                                        [ class "f4 ph3 pv3 bb b--light-silver dim pointer"
                                        , onClick <| ChooseApartmentFor (Just (Choose a)) toMsg
                                        ]
                                        [ text a.name ]
                                )
                                model.apartments
                            )
                        ]
                    ]

            Nothing ->
                text ""
        , Carousel.carousel
            ScrollTo
            (Dict.size model.weeks)
            model.carouselIdx
            [ style "flex" "1"
            , id "calendar-scroller"
            ]
            [ div
                [ style "height" "100%"
                , style "position" "relative"
                , style "display" "flex"
                , style "flex-direction" "row"
                , style "flex-wrap" "nowrap"
                ]
                ((Dict.toList model.weeks
                    |> List.indexedMap
                        (\i ( _, week ) ->
                            viewWeek model (String.fromInt i) week
                        )
                 )
                    ++ [ div
                            [ class "h-100"
                            , style "min-width" "100vw"
                            , style "scroll-snap-align" "start"
                            , style "scroll-snap-stop" "normal"
                            ]
                            []
                       ]
                )
            ]
        ]
