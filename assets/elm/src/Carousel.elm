module Carousel exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D


type alias Scroll =
    { scrollHeight : Float
    , scrollLeft : Float
    , scrollLeftMax : Float
    , scrollTop : Float
    , scrollTopMax : Float
    , scrollWidth : Float
    }


carousel map pages index attr elements =
    node "elm-carousel"
        ([ style "overflow-scrolling" "touch"
         , style "display" "block"
         , style "-webkit-overflow-scrolling" "touch"
         , style "overflow-anchor" "none"
         , style "overflow-x" "scroll"
         , style "overflow-y" "auto"
         , style "scroll-snap-type" "x mandatory"
         , style "scroll-snap-stop" "normal"
         , style "scroll-behavior" "auto"
         , attribute "index" (String.fromInt index)
         , attribute "pages" (String.fromInt pages)
         , on "index-changed"
            (D.field "target"
                (D.field "_index"
                    D.int
                )
                |> D.map map
            )
         ]
            ++ attr
        )
        elements
