module History exposing (Action(..), History, decoder)

import Apartment
import Clock
import Date exposing (Date)
import Json.Decode as D
import Schedule


type Action
    = Booked
    | UnBooked


actionDecoder : D.Decoder Action
actionDecoder =
    D.field "action" D.string
        |> D.andThen
            (\str ->
                case str of
                    "book" ->
                        D.succeed Booked

                    "unbook" ->
                        D.succeed UnBooked

                    _ ->
                        D.fail <| "\"" ++ str ++ "\" didn't match any action types"
            )


type alias History =
    { action : Action
    , apartment : Apartment.Apartment
    , to : Clock.Clock
    , from : Clock.Clock
    , happened : String
    , date : Date
    }


decoder : D.Decoder History
decoder =
    D.map6 History
        actionDecoder
        (D.field "apartment" Apartment.decoder)
        (D.field "to" Schedule.clockDecoder)
        (D.field "from" Schedule.clockDecoder)
        (D.field "happened" D.string)
        (D.field "date" Schedule.dateDecoder)
