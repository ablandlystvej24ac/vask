module Clock exposing (Clock, fromString, toString)

import Parser as P exposing ((|.), (|=))


type alias Clock =
    { hours : Int
    , minutes : Int
    , seconds : Int
    }


fromString : String -> Result String Clock
fromString clock =
    let
        num =
            P.andThen
                (\str ->
                    case String.toInt str of
                        Just i ->
                            P.succeed i

                        Nothing ->
                            P.problem ("Could not parse " ++ str ++ " as an integer")
                )
            <|
                P.getChompedString <|
                    P.succeed ()
                        |. P.chompWhile Char.isDigit
    in
    P.run
        (P.succeed Clock
            |= num
            |. P.symbol ":"
            |= num
            |. P.symbol ":"
            |= num
        )
        clock
        |> Result.mapError (always clock)


toString : Clock -> String
toString clock =
    (String.fromInt clock.hours
        |> String.pad 2 '0'
    )
        ++ ":"
        ++ (String.fromInt clock.minutes
                |> String.pad 2 '0'
           )
