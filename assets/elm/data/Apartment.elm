module Apartment exposing (Apartment, decoder, encoder)

import Json.Decode as D
import Json.Encode as E


type alias Apartment =
    { id : Int
    , name : String
    }


decoder : D.Decoder Apartment
decoder =
    D.map2 Apartment
        (D.field "id" D.int)
        (D.field "name" D.string)


encoder : Apartment -> E.Value
encoder a =
    E.object
        [ ( "id", E.int a.id )
        , ( "name", E.string a.name )
        ]
