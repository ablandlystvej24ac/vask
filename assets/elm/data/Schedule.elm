module Schedule exposing (Schedule, Status(..), clockDecoder, dateDecoder, decoder)

import Apartment
import Clock
import Date
import Json.Decode as D


type Status
    = Available
    | Booked Apartment.Apartment
    | Loading
    | Error


statusDecoder : D.Decoder Status
statusDecoder =
    D.map2
        (\status apartment ->
            case status of
                "available" ->
                    Ok Available

                "booked" ->
                    case apartment of
                        Just a ->
                            Ok <| Booked a

                        Nothing ->
                            Err "Expected apartment with booking but there was none"

                _ ->
                    Err <| "\"" ++ status ++ "\" didn't match any status types"
        )
        (D.field "status" D.string)
        (D.maybe <| D.field "apartment" Apartment.decoder)
        |> D.andThen
            (\res ->
                case res of
                    Ok d ->
                        D.succeed d

                    Err e ->
                        D.fail e
            )


dateDecoder : D.Decoder Date.Date
dateDecoder =
    D.string
        |> D.andThen
            (\str ->
                case Date.fromIsoString str of
                    Ok d ->
                        D.succeed d

                    Err e ->
                        D.fail e
            )


clockDecoder : D.Decoder Clock.Clock
clockDecoder =
    D.string
        |> D.andThen
            (\str ->
                case Clock.fromString str of
                    Ok c ->
                        D.succeed c

                    Err e ->
                        D.fail e
            )


type alias Schedule =
    { from : Clock.Clock
    , to : Clock.Clock
    , scheduleId : Int
    , status : Status
    }


decoder : D.Decoder Schedule
decoder =
    D.map4 Schedule
        (D.field "from" clockDecoder)
        (D.field "to" clockDecoder)
        (D.field "schedule_id" D.int)
        (D.field "booking" statusDecoder)
