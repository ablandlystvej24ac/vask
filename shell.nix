{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [
    pkgs.sqlite 
    pkgs.elixir 
    pkgs.inotify-tools
    pkgs.elmPackages.elm
    pkgs.elmPackages.elm-format
    pkgs.elmPackages.elm-test
    pkgs.elmPackages.elm-review
    pkgs.elmPackages.elm-language-server
    pkgs.esbuild
  ];
  shellHook = ''
    export LANG=en_US.UTF-8
    export DATABASE_PATH=./vask_dev.db
    export SECRET_ADMIN_HASH='$2b$12$gtP9tJM9OLI5.Mwozc11EO8RvlBcOD.lyw3OAL/caNrtKQQFXDZsG'
    export SECRET_KEY_BASE=RwPtce8EALJQSSgxGlLSkz9BH/mIqwylizwid6hL0hBtykn9/oxxo3WICPPnhu4u
    mix local.hex --if-missing
  '';
}
