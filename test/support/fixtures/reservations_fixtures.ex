defmodule Vask.ReservationsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Vask.Reservations` context.
  """

  @doc """
  Generate a reservation.
  """
  def reservation_fixture(attrs \\ %{}) do
    {:ok, reservation} =
      attrs
      |> Enum.into(%{

      })
      |> Vask.Reservations.create_reservation()

    reservation
  end
end
