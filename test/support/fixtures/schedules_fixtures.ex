defmodule Vask.SchedulesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Vask.Schedules` context.
  """

  @doc """
  Generate a schedule.
  """
  def schedule_fixture(attrs \\ %{}) do
    {:ok, schedule} =
      attrs
      |> Enum.into(%{
        from: ~T[14:00:00],
        to: ~T[14:00:00]
      })
      |> Vask.Schedules.create_schedule()

    schedule
  end
end
