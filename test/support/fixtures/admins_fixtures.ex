defmodule Vask.AdminsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Vask.Admins` context.
  """

  def admin_fixture(_attrs \\ %{}) do
    %{ token: Vask.Admins.generate_admin_session_token(),
       password: "Kode12345678"
     }
  end
end
