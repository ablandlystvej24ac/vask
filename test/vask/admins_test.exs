defmodule Vask.AdminsTest do
  use Vask.DataCase

  alias Vask.Admins

  import Vask.AdminsFixtures
  alias Vask.Admins.{Admin, AdminToken}


  describe "generate_admin_session_token/1" do
    test "generates a token" do
      token = Admins.generate_admin_session_token()
      assert admin_token = Repo.get_by(AdminToken, token: token)
      assert admin_token.context == "admin_session"

      # Creating the same token for another admin should fail
      assert_raise Ecto.ConstraintError, fn ->
        Repo.insert!(%AdminToken{
          token: admin_token.token,
          context: "admin_session"
        })
      end
    end
  end
end
