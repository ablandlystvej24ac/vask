#!/bin/bash

cd ./assets/elm
./compile.sh
cd -

mix deps.get --only prod
MIX_ENV=prod mix compile
MIX_ENV=prod mix assets.deploy
MIX_ENV=prod mix release
mix phx.gen.release --docker

docker build --no-cache -t vask:0.0.3 .
