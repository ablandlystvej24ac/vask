#!/bin/sh

cd assets/elm

yarn install
./node_modules/elm/bin/elm make src/Main.elm --output=build/elm.js 
./node_modules/esbuild/bin/esbuild --minify --pure:A2 --pure:A3 --pure:A4 --pure:A5 --pure:A6 --pure:A7 --pure:A8 --pure:A9 --pure:F2 --pure:F3 --pure:F3 --pure:F4 --pure:F5 --pure:F6 --pure:F7 --pure:F8 --pure:F9 --outfile=../../priv/static/elm/elm.js build/elm.js

